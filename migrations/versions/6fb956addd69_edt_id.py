"""edt_id

Revision ID: 6fb956addd69
Revises: fd805feb7ba8
Create Date: 2023-11-06 12:14:42.808476

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "6fb956addd69"
down_revision = "fd805feb7ba8"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("group_descr", schema=None) as batch_op:
        batch_op.add_column(sa.Column("edt_id", sa.Text(), nullable=True))
        batch_op.create_index(
            batch_op.f("ix_group_descr_edt_id"), ["edt_id"], unique=False
        )

    with op.batch_alter_table("notes_formsemestre", schema=None) as batch_op:
        batch_op.add_column(sa.Column("edt_id", sa.Text(), nullable=True))
        batch_op.create_index(
            batch_op.f("ix_notes_formsemestre_edt_id"), ["edt_id"], unique=False
        )

    with op.batch_alter_table("notes_modules", schema=None) as batch_op:
        batch_op.add_column(sa.Column("edt_id", sa.Text(), nullable=True))
        batch_op.create_index(
            batch_op.f("ix_notes_modules_edt_id"), ["edt_id"], unique=False
        )

    with op.batch_alter_table("user", schema=None) as batch_op:
        batch_op.add_column(sa.Column("edt_id", sa.Text(), nullable=True))
        batch_op.create_index(batch_op.f("ix_user_edt_id"), ["edt_id"], unique=False)


def downgrade():
    with op.batch_alter_table("user", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_user_edt_id"))
        batch_op.drop_column("edt_id")

    with op.batch_alter_table("notes_modules", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_notes_modules_edt_id"))
        batch_op.drop_column("edt_id")

    with op.batch_alter_table("notes_formsemestre", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_notes_formsemestre_edt_id"))
        batch_op.drop_column("edt_id")

    with op.batch_alter_table("group_descr", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_group_descr_edt_id"))
        batch_op.drop_column("edt_id")
