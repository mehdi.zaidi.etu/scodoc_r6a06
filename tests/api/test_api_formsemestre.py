# -*- coding: utf-8 -*-

"""Test formsemestre

Utilisation :
    créer les variables d'environnement: (indiquer les valeurs
    pour le serveur ScoDoc que vous voulez interroger)

    export SCODOC_URL="https://scodoc.xxx.net/"
    export SCODOC_USER="xxx"
    export SCODOC_PASSWD="xxx"
    export CHECK_CERTIFICATE=0 # ou 1 si serveur de production avec certif SSL valide

    (on peut aussi placer ces valeurs dans un fichier .env du répertoire tests/api).

    Lancer :
        pytest tests/api/test_api_formsemestre.py
"""
import json
import requests
from types import NoneType

from app.scodoc import sco_utils as scu

from tests.api.setup_test_api import (
    API_URL,
    CHECK_CERTIFICATE,
    GET,
    POST_JSON,
    api_headers,
)

from tests.api.tools_test_api import (
    verify_fields,
    MODIMPL_FIELDS,
    EVAL_FIELDS,
    SAISIE_NOTES_FIELDS,
    FORMSEMESTRE_ETUD_FIELDS,
    FSEM_FIELDS,
    UE_FIELDS,
    FORMSEMESTRE_BULLETINS_FIELDS,
    FORMSEMESTRE_BULLETINS_ETU_FIELDS,
    FORMSEMESTRE_BULLETINS_FORMATION_FIELDS,
    FORMSEMESTRE_BULLETINS_OPT_FIELDS,
    BULLETIN_RESSOURCES_ET_SAES_RESSOURCE_ET_SAE_FIELDS,
    BULLETIN_RESSOURCES_ET_SAES_RESSOURCE_ET_SAE_EVALUATION_FIELDS,
    BULLETIN_RESSOURCES_ET_SAES_RESSOURCE_ET_SAE_EVALUATION_POIDS_FIELDS,
    BULLETIN_RESSOURCES_ET_SAES_RESSOURCE_ET_SAE_EVALUATION_NOTE_FIELDS,
    BULLETIN_UES_UE_FIELDS,
    BULLETIN_UES_UE_MOYENNE_FIELDS,
    BULLETIN_UES_UE_RESSOURCES_RESSOURCE_FIELDS,
    BULLETIN_UES_UE_SAES_SAE_FIELDS,
    BULLETIN_UES_UE_ECTS_FIELDS,
    BULLETIN_SEMESTRE_FIELDS,
    BULLETIN_SEMESTRE_ABSENCES_FIELDS,
    BULLETIN_SEMESTRE_ECTS_FIELDS,
    BULLETIN_SEMESTRE_NOTES_FIELDS,
    BULLETIN_SEMESTRE_RANG_FIELDS,
)

# Etudiant pour les tests
ETUDID = 1
NIP = "1"
INE = "INE1"


def test_formsemestre(api_headers):
    """
    Route: /formsemestre/<id>
    """
    formsemestre_id = 1
    r = requests.get(
        f"{API_URL}/formsemestre/{formsemestre_id}",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 200
    formsemestre = r.json()
    assert verify_fields(formsemestre, FSEM_FIELDS)
    assert isinstance(formsemestre["block_moyennes"], bool)
    assert isinstance(formsemestre["block_moyenne_generale"], bool)
    assert isinstance(formsemestre["bul_bgcolor"], str)
    assert isinstance(formsemestre["bul_hide_xml"], bool)
    assert isinstance(formsemestre["date_debut_iso"], str)
    assert isinstance(formsemestre["date_debut"], str)
    assert isinstance(formsemestre["date_fin_iso"], str)
    assert isinstance(formsemestre["date_fin"], str)
    assert isinstance(formsemestre["dept_id"], int)
    assert formsemestre["elt_annee_apo"] is None or isinstance(
        formsemestre["elt_annee_apo"], str
    )
    assert formsemestre["elt_sem_apo"] is None or isinstance(
        formsemestre["elt_sem_apo"], str
    )
    assert isinstance(formsemestre["ens_can_edit_eval"], bool)
    assert isinstance(formsemestre["etat"], bool)
    assert isinstance(formsemestre["formation_id"], int)
    assert isinstance(formsemestre["formsemestre_id"], int)
    assert isinstance(formsemestre["gestion_compensation"], bool)
    assert isinstance(formsemestre["gestion_semestrielle"], bool)
    assert isinstance(formsemestre["id"], int)
    assert isinstance(formsemestre["modalite"], str)
    assert isinstance(formsemestre["resp_can_change_ens"], bool)
    assert isinstance(formsemestre["resp_can_edit"], bool)
    assert isinstance(formsemestre["responsables"], list)
    assert formsemestre["scodoc7_id"] is None or isinstance(
        formsemestre["scodoc7_id"], int
    )
    assert isinstance(formsemestre["semestre_id"], int)
    assert isinstance(formsemestre["titre_num"], str)
    assert isinstance(formsemestre["titre"], str)

    ### ERROR ###
    formsemestre_id_inexistant = 165456165165136513510351
    r = requests.get(
        f"{API_URL}/formsemestre/{formsemestre_id_inexistant}",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 404


def test_formsemestre_apo(api_headers):
    """
    Route: formsemestres/query?etape_apo=<string:etape_apo>
    """
    etape_apo = "A1"
    r = requests.get(
        f"{API_URL}/formsemestres/query?etape_apo={etape_apo}",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 200
    list_formsemestre = r.json()
    assert isinstance(list_formsemestre, list)

    for formsemestre in list_formsemestre:
        assert isinstance(formsemestre, dict)
        assert verify_fields(formsemestre, FSEM_FIELDS)
        assert isinstance(formsemestre["block_moyennes"], bool)
        assert isinstance(formsemestre["block_moyenne_generale"], bool)
        assert isinstance(formsemestre["bul_bgcolor"], str)
        assert isinstance(formsemestre["bul_hide_xml"], bool)
        assert isinstance(formsemestre["date_debut_iso"], str)
        assert isinstance(formsemestre["date_debut"], str)
        assert isinstance(formsemestre["date_fin_iso"], str)
        assert isinstance(formsemestre["date_fin"], str)
        assert isinstance(formsemestre["dept_id"], int)
        assert formsemestre["elt_annee_apo"] is None or isinstance(
            formsemestre["elt_annee_apo"], str
        )
        assert formsemestre["elt_sem_apo"] is None or isinstance(
            formsemestre["elt_sem_apo"], str
        )
        assert isinstance(formsemestre["ens_can_edit_eval"], bool)
        assert isinstance(formsemestre["etat"], bool)
        assert isinstance(formsemestre["formation_id"], int)
        assert isinstance(formsemestre["formsemestre_id"], int)
        assert isinstance(formsemestre["gestion_compensation"], bool)
        assert isinstance(formsemestre["gestion_semestrielle"], bool)
        assert isinstance(formsemestre["id"], int)
        assert isinstance(formsemestre["modalite"], str)
        assert isinstance(formsemestre["resp_can_change_ens"], bool)
        assert isinstance(formsemestre["resp_can_edit"], bool)
        assert isinstance(formsemestre["responsables"], list)
        assert formsemestre["scodoc7_id"] is None or isinstance(
            formsemestre["scodoc7_id"], int
        )
        assert isinstance(formsemestre["semestre_id"], int)
        assert isinstance(formsemestre["titre_num"], str)
        assert isinstance(formsemestre["titre"], str)

    ### ERROR ###
    # etape_apo_inexistante = "aoefiaozidaoẑidjnoaiznjd"
    # r_error = requests.get(
    #     f"{API_URL}/formsemestre/apo/{etape_apo_inexistante}",
    #     headers=api_headers,
    #     verify=CHECK_CERTIFICATE,
    # )
    # assert r_error.status_code == 404


def test_bulletins(api_headers):
    """
    Route: /formsemestre/<int:formsemestre_id>/bulletins
    """
    formsemestre_id = 1
    r = requests.get(
        f"{API_URL}/formsemestre/{formsemestre_id}/bulletins",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 200

    bulletins = r.json()

    assert isinstance(bulletins, list)
    for bul in bulletins:
        assert verify_fields(bul, FORMSEMESTRE_BULLETINS_FIELDS) is True
        assert isinstance(bul["version"], str)
        assert isinstance(bul["type"], str)
        assert isinstance(bul["date"], str)
        assert isinstance(bul["publie"], bool)
        assert isinstance(bul["etudiant"], dict)
        assert isinstance(bul["formation"], dict)
        assert isinstance(bul["formsemestre_id"], int)
        assert isinstance(bul["etat_inscription"], str)
        assert isinstance(bul["options"], dict)
        assert isinstance(bul["ressources"], dict)
        assert isinstance(bul["saes"], dict)
        assert isinstance(bul["ues"], dict)
        assert isinstance(bul["semestre"], dict)

        formsemestre_id_bul = bul["formsemestre_id"]
        assert formsemestre_id == formsemestre_id_bul

        etudiant = bul["etudiant"]
        assert verify_fields(etudiant, FORMSEMESTRE_BULLETINS_ETU_FIELDS) is True
        assert isinstance(etudiant["civilite"], str)
        assert isinstance(etudiant["code_ine"], str)
        assert isinstance(etudiant["code_nip"], str)
        assert isinstance(etudiant["date_naissance"], str)
        assert isinstance(etudiant["dept_id"], int)
        assert isinstance(etudiant["dept_acronym"], str)
        assert isinstance(etudiant["email"], str)
        assert isinstance(etudiant["emailperso"], str)
        assert isinstance(etudiant["etudid"], int)
        assert isinstance(etudiant["nom"], str)
        assert isinstance(etudiant["prenom"], str)
        assert isinstance(etudiant["nomprenom"], str)
        assert isinstance(etudiant["lieu_naissance"], str)
        assert isinstance(etudiant["dept_naissance"], str)
        assert isinstance(etudiant["nationalite"], str)
        assert isinstance(etudiant["boursier"], str)
        assert isinstance(etudiant["fiche_url"], str)
        assert isinstance(etudiant["photo_url"], str)
        assert isinstance(etudiant["id"], int)
        assert isinstance(etudiant["codepostaldomicile"], str)
        assert isinstance(etudiant["paysdomicile"], str)
        assert isinstance(etudiant["telephonemobile"], str)
        assert isinstance(etudiant["typeadresse"], str)
        assert isinstance(etudiant["domicile"], str)
        assert isinstance(etudiant["villedomicile"], str)
        assert isinstance(etudiant["telephone"], str)
        assert isinstance(etudiant["fax"], str)
        assert isinstance(etudiant["description"], str)

        formation = bul["formation"]
        assert verify_fields(formation, FORMSEMESTRE_BULLETINS_FORMATION_FIELDS) is True
        assert isinstance(formation["id"], int)
        assert isinstance(formation["acronyme"], str)
        assert isinstance(formation["titre_officiel"], str)
        assert isinstance(formation["titre"], str)

        options = bul["options"]
        assert verify_fields(options, FORMSEMESTRE_BULLETINS_OPT_FIELDS) is True
        assert isinstance(options["show_abs"], bool)
        assert isinstance(options["show_abs_modules"], bool)
        assert isinstance(options["show_ects"], bool)
        assert isinstance(options["show_codemodules"], bool)
        assert isinstance(options["show_matieres"], bool)
        assert isinstance(options["show_rangs"], bool)
        assert isinstance(options["show_ue_rangs"], bool)
        assert isinstance(options["show_mod_rangs"], bool)
        assert isinstance(options["show_moypromo"], bool)
        assert isinstance(options["show_minmax"], bool)
        assert isinstance(options["show_minmax_mod"], bool)
        assert isinstance(options["show_minmax_eval"], bool)
        assert isinstance(options["show_coef"], bool)
        assert isinstance(options["show_ue_cap_details"], bool)
        assert isinstance(options["show_ue_cap_current"], bool)
        assert isinstance(options["show_temporary"], bool)
        assert isinstance(options["temporary_txt"], str)
        assert isinstance(options["show_uevalid"], bool)
        assert isinstance(options["show_date_inscr"], bool)

        bulletin_ressources = bul["ressources"]
        assert isinstance(bulletin_ressources, dict)

        for ressource in bulletin_ressources.values():
            assert (
                verify_fields(
                    ressource, BULLETIN_RESSOURCES_ET_SAES_RESSOURCE_ET_SAE_FIELDS
                )
                is True
            )
            assert isinstance(ressource, dict)
            assert isinstance(ressource["evaluations"], list)
            for evaluation in ressource["evaluations"]:
                assert (
                    verify_fields(
                        evaluation,
                        BULLETIN_RESSOURCES_ET_SAES_RESSOURCE_ET_SAE_EVALUATION_FIELDS,
                    )
                    is True
                )
                assert isinstance(evaluation["id"], int)
                assert evaluation["description"] is None or isinstance(
                    evaluation["description"], str
                )
                assert isinstance(evaluation["date_debut"], (str, NoneType))
                assert isinstance(evaluation["date_fin"], (str, NoneType))
                assert isinstance(evaluation["coef"], (str, NoneType))
                assert isinstance(evaluation["poids"], dict)
                assert isinstance(evaluation["note"], dict)
                assert isinstance(evaluation["url"], str)

                # Deprecated (supprimer avant #sco9.7):
                assert isinstance(evaluation["date"], (str, NoneType))
                assert isinstance(evaluation["heure_debut"], (str, NoneType))
                assert isinstance(evaluation["heure_fin"], (str, NoneType))

                assert (
                    verify_fields(
                        evaluation["poids"],
                        BULLETIN_RESSOURCES_ET_SAES_RESSOURCE_ET_SAE_EVALUATION_POIDS_FIELDS,
                    )
                    is True
                )
                assert isinstance(evaluation["poids"]["RT1.1"], float)
                assert isinstance(evaluation["poids"]["RT2.1"], float)
                assert isinstance(evaluation["poids"]["RT3.1"], float)

                assert (
                    verify_fields(
                        evaluation["note"],
                        BULLETIN_RESSOURCES_ET_SAES_RESSOURCE_ET_SAE_EVALUATION_NOTE_FIELDS,
                    )
                    is True
                )
                assert isinstance(evaluation["note"]["value"], str)
                assert isinstance(evaluation["note"]["min"], str)
                assert isinstance(evaluation["note"]["max"], str)
                assert isinstance(evaluation["note"]["moy"], str)

        bulletin_saes = bul["saes"]
        assert isinstance(bulletin_saes, dict)

        for sae in bulletin_saes.values():
            assert (
                verify_fields(sae, BULLETIN_RESSOURCES_ET_SAES_RESSOURCE_ET_SAE_FIELDS)
                is True
            )
            assert isinstance(sae, dict)
            assert isinstance(sae["evaluations"], list)
            for evaluation in sae["evaluations"]:
                assert (
                    verify_fields(
                        evaluation,
                        BULLETIN_RESSOURCES_ET_SAES_RESSOURCE_ET_SAE_EVALUATION_FIELDS,
                    )
                    is True
                )
                assert isinstance(evaluation["id"], int)
                assert evaluation["description"] is None or isinstance(
                    evaluation["description"], str
                )
                assert isinstance(evaluation["date_debut"], (str, NoneType))
                assert isinstance(evaluation["date_fin"], (str, NoneType))
                assert isinstance(evaluation["coef"], str)
                assert isinstance(evaluation["poids"], dict)
                assert isinstance(evaluation["note"], dict)
                assert isinstance(evaluation["url"], str)

                # Deprecated (supprimer avant #sco9.7):
                assert isinstance(evaluation["date"], (str, NoneType))
                assert isinstance(evaluation["heure_debut"], (str, NoneType))
                assert isinstance(evaluation["heure_fin"], (str, NoneType))

                assert (
                    verify_fields(
                        evaluation["poids"],
                        BULLETIN_RESSOURCES_ET_SAES_RESSOURCE_ET_SAE_EVALUATION_POIDS_FIELDS,
                    )
                    is True
                )
                assert isinstance(evaluation["poids"]["RT1.1"], float)
                assert isinstance(evaluation["poids"]["RT2.1"], float)
                assert isinstance(evaluation["poids"]["RT3.1"], float)

                assert (
                    verify_fields(
                        evaluation["note"],
                        BULLETIN_RESSOURCES_ET_SAES_RESSOURCE_ET_SAE_EVALUATION_NOTE_FIELDS,
                    )
                    is True
                )
                assert isinstance(evaluation["note"]["value"], str)
                assert isinstance(evaluation["note"]["min"], str)
                assert isinstance(evaluation["note"]["max"], str)
                assert isinstance(evaluation["note"]["moy"], str)

        bulletin_ues = bul["ues"]
        assert isinstance(bulletin_ues, dict)

        for key_ue, value_ue in bulletin_ues.items():
            assert verify_fields(value_ue, BULLETIN_UES_UE_FIELDS) is True
            assert isinstance(value_ue["id"], int)
            assert isinstance(value_ue["titre"], str)
            assert isinstance(value_ue["numero"], int)
            assert isinstance(value_ue["type"], int)
            assert isinstance(value_ue["color"], str)
            assert value_ue["competence"] is None or isinstance(
                value_ue["competence"], str
            )
            assert isinstance(value_ue["moyenne"], dict)
            assert isinstance(value_ue["bonus"], str)
            assert isinstance(value_ue["malus"], str)
            assert value_ue["capitalise"] is None or isinstance(
                value_ue["capitalise"], str
            )
            assert isinstance(value_ue["ressources"], dict)
            assert isinstance(value_ue["saes"], dict)
            assert isinstance(value_ue["ECTS"], dict)

            assert (
                verify_fields(value_ue["moyenne"], BULLETIN_UES_UE_MOYENNE_FIELDS)
                is True
            )
            assert isinstance(value_ue["moyenne"]["value"], str)
            assert isinstance(value_ue["moyenne"]["min"], str)
            assert isinstance(value_ue["moyenne"]["max"], str)
            assert isinstance(value_ue["moyenne"]["moy"], str)
            assert isinstance(value_ue["moyenne"]["rang"], str)
            assert isinstance(value_ue["moyenne"]["total"], int)

            for ressource in value_ue["ressources"].values():
                assert (
                    verify_fields(
                        ressource, BULLETIN_UES_UE_RESSOURCES_RESSOURCE_FIELDS
                    )
                    is True
                )
                assert isinstance(ressource["id"], int)
                assert isinstance(ressource["coef"], float)
                assert isinstance(ressource["moyenne"], str)

            for sae in value_ue["saes"].values():
                assert verify_fields(sae, BULLETIN_UES_UE_SAES_SAE_FIELDS) is True
                assert isinstance(sae["id"], int)
                assert isinstance(sae["coef"], float)
                assert isinstance(sae["moyenne"], str)

            assert verify_fields(value_ue["ECTS"], BULLETIN_UES_UE_ECTS_FIELDS) is True
            assert isinstance(value_ue["ECTS"]["acquis"], float)
            assert isinstance(value_ue["ECTS"]["total"], float)

        bulletin_semestre = bul["semestre"]
        assert verify_fields(bulletin_semestre, BULLETIN_SEMESTRE_FIELDS) is True
        assert isinstance(bulletin_semestre["etapes"], list)
        assert isinstance(bulletin_semestre["date_debut"], str)
        assert isinstance(bulletin_semestre["date_fin"], str)
        assert isinstance(bulletin_semestre["annee_universitaire"], str)
        assert isinstance(bulletin_semestre["numero"], int)
        assert isinstance(bulletin_semestre["inscription"], str)
        assert isinstance(bulletin_semestre["groupes"], list)
        assert isinstance(bulletin_semestre["absences"], dict)
        assert isinstance(bulletin_semestre["ECTS"], dict)
        assert isinstance(bulletin_semestre["notes"], dict)
        assert isinstance(bulletin_semestre["rang"], dict)

        assert (
            verify_fields(
                bulletin_semestre["absences"], BULLETIN_SEMESTRE_ABSENCES_FIELDS
            )
            is True
        )
        assert isinstance(bulletin_semestre["absences"]["injustifie"], int)
        assert isinstance(bulletin_semestre["absences"]["total"], int)

        assert (
            verify_fields(bulletin_semestre["ECTS"], BULLETIN_SEMESTRE_ECTS_FIELDS)
            is True
        )
        assert isinstance(bulletin_semestre["ECTS"]["acquis"], float)
        assert isinstance(bulletin_semestre["ECTS"]["total"], float)

        assert (
            verify_fields(bulletin_semestre["notes"], BULLETIN_SEMESTRE_NOTES_FIELDS)
            is True
        )
        assert isinstance(bulletin_semestre["notes"]["value"], str)
        assert isinstance(bulletin_semestre["notes"]["min"], str)
        assert isinstance(bulletin_semestre["notes"]["max"], str)
        assert isinstance(bulletin_semestre["notes"]["moy"], str)

        assert (
            verify_fields(bulletin_semestre["rang"], BULLETIN_SEMESTRE_RANG_FIELDS)
            is True
        )
        assert isinstance(bulletin_semestre["rang"]["value"], str)
        assert isinstance(bulletin_semestre["rang"]["total"], int)


def test_formsemestre_etudiants(api_headers):
    """
    Route:  /formsemestre/<int:formsemestre_id>/etudiants
            /formsemestre/<int:formsemestre_id>/etudiants/actifs
            /formsemestre/<int:formsemestre_id>/etudiants/demissionnaires
            /formsemestre/<int:formsemestre_id>/etudiants/defaillants
    """
    formsemestre_id = 1
    etuds = GET(f"/formsemestre/{formsemestre_id}/etudiants", headers=api_headers)
    assert isinstance(etuds, list)
    for etud in etuds:
        assert verify_fields(etud, FORMSEMESTRE_ETUD_FIELDS) is True
        assert isinstance(etud["id"], int)
        assert isinstance(etud["code_nip"], str)
        assert isinstance(etud["code_ine"], str)
        assert isinstance(etud["nom"], str)
        assert etud["nom_usuel"] is None or isinstance(etud["nom_usuel"], str)
        assert isinstance(etud["prenom"], str)
        assert isinstance(etud["civilite"], str)
        assert isinstance(etud["groups"], list)
        etu_groups = etud["groups"]
        for group in etu_groups:
            assert isinstance(group["partition_id"], int)
            assert isinstance(group["id"], int)
            assert isinstance(group["formsemestre_id"], int)
            assert group["partition_name"] is None or isinstance(
                group["partition_name"], str
            )
            assert (group["numero"] is None) or isinstance(group["numero"], int)
            assert isinstance(group["bul_show_rank"], bool)
            assert isinstance(group["show_in_lists"], bool)
            assert isinstance(group["group_id"], int)
            assert group["group_name"] is None or isinstance(group["group_name"], int)

    ## Avec query:
    etuds_query = GET(
        f"/formsemestre/{formsemestre_id}/etudiants/query", headers=api_headers
    )
    assert etuds_query == etuds

    ### actifs
    etuds_actifs = GET(
        f"/formsemestre/{formsemestre_id}/etudiants/query?etat=I", headers=api_headers
    )
    assert isinstance(etuds_actifs, list)

    ### démissionnaires
    etuds_dem = GET(
        f"/formsemestre/{formsemestre_id}/etudiants/query?etat=D",
        headers=api_headers,
    )
    assert isinstance(etuds_dem, list)

    ### défaillants
    etuds_def = GET(
        f"/formsemestre/{formsemestre_id}/etudiants/query?etat=DEF", headers=api_headers
    )
    assert isinstance(etuds_def, list)

    assert len(etuds) == (len(etuds_actifs) + len(etuds_dem) + len(etuds_def))

    ### ERROR ###
    id_formsemestre_inexistant = 265165689619851621685
    r_error = requests.get(
        f"{API_URL}/formsemestre/{id_formsemestre_inexistant}/etudiants",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r_error.status_code == 404

    r_error_demissionnaires = requests.get(
        f"{API_URL}/formsemestre/{id_formsemestre_inexistant}/etudiants/demissionnaires",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r_error_demissionnaires.status_code == 404

    r_error_defaillants = requests.get(
        f"{API_URL}/formsemestre/{id_formsemestre_inexistant}/etudiants/defaillants",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r_error_defaillants.status_code == 404


def test_formsemestre_programme(api_headers):
    """
    Route:  /formsemestre/1/programme
    """

    r = requests.get(
        API_URL + "/formsemestre/1/programme",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 200
    prog = r.json()
    assert isinstance(prog, dict)
    assert "ues" in prog
    assert "modules" in prog
    assert "ressources" in prog
    assert "saes" in prog
    assert isinstance(prog["ues"], list)
    assert isinstance(prog["modules"], list)
    ue = prog["ues"][0]
    modules = prog["modules"]
    # Il y a toujours au moins une SAE et une ressources dans notre base de test
    ressource = prog["ressources"][0]
    sae = prog["saes"][0]

    assert verify_fields(ue, UE_FIELDS)
    if len(modules) > 1:
        assert verify_fields(modules[0], MODIMPL_FIELDS)
    assert verify_fields(ressource, MODIMPL_FIELDS)
    assert verify_fields(sae, MODIMPL_FIELDS)


def test_etat_evals(api_headers):
    """
    Route : /formsemestre/<int:formsemestre_id>/etat_evals
    """
    formsemestre_id = 1
    headers = api_headers
    etat_evals = GET(f"/formsemestre/{formsemestre_id}/etat_evals", headers=headers)

    assert len(etat_evals) == 21

    for etat_modimpl in etat_evals:
        assert isinstance(etat_modimpl["id"], int)
        assert isinstance(etat_modimpl["evaluations"], list)

        for evaluation in etat_modimpl["evaluations"]:
            assert verify_fields(evaluation, EVAL_FIELDS)
            assert isinstance(evaluation["id"], int)
            assert evaluation["description"] is None or isinstance(
                evaluation["description"], str
            )
            assert evaluation["date_debut"] is None or isinstance(
                evaluation["date_debut"], str
            )
            assert evaluation["date_fin"] is None or isinstance(
                evaluation["date_fin"], str
            )
            assert isinstance(evaluation["coefficient"], float)
            assert isinstance(evaluation["etat"], dict)
            assert isinstance(evaluation["nb_inscrits"], int)
            assert isinstance(evaluation["nb_notes_manquantes"], int)
            assert isinstance(evaluation["nb_notes_abs"], int)
            assert isinstance(evaluation["nb_notes_att"], int)
            assert isinstance(evaluation["nb_notes_exc"], int)
            assert isinstance(evaluation["saisie_notes"], dict)

            list_eval_id = [e["id"] for e in etat_modimpl["evaluations"]]
            all_unique = True
            for id in list_eval_id:
                if list_eval_id.count(id) > 1:
                    all_unique = False
            assert all_unique is True

            saisie_notes = evaluation["saisie_notes"]
            assert verify_fields(saisie_notes, SAISIE_NOTES_FIELDS)
            assert evaluation["saisie_notes"]["datetime_debut"] is None or isinstance(
                evaluation["saisie_notes"]["datetime_debut"], str
            )
            assert evaluation["saisie_notes"]["datetime_debut"] is None or isinstance(
                evaluation["saisie_notes"]["datetime_fin"], str
            )
            assert evaluation["saisie_notes"]["datetime_debut"] is None or isinstance(
                evaluation["saisie_notes"]["datetime_mediane"], str
            )

            if (
                evaluation["saisie_notes"]["datetime_fin"] is not None
                and evaluation["saisie_notes"]["datetime_mediane"] is not None
                and evaluation["saisie_notes"]["datetime_debut"] is not None
            ):
                assert (
                    evaluation["saisie_notes"]["datetime_fin"]
                    > evaluation["saisie_notes"]["datetime_mediane"]
                )
                assert (
                    evaluation["saisie_notes"]["datetime_fin"]
                    > evaluation["saisie_notes"]["datetime_debut"]
                )
                assert (
                    evaluation["saisie_notes"]["datetime_mediane"]
                    > evaluation["saisie_notes"]["datetime_debut"]
                )

    ##### ERROR #####
    invalid_id = 153165161656849846516511321651651
    r = requests.get(
        f"{API_URL}/formsemestre/{invalid_id}/etat_evals",
        headers=headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 404


def test_formsemestre_resultat(api_headers):
    """
    Route:  /formsemestre/<int:formsemestre_id>/resultats
    """
    # Test brutal: compare les texts des json (après suppression des espaces et tabs)
    # ce test cassera à la moindre modification :-)
    formsemestre_id = 1
    r = requests.get(
        f"{API_URL}/formsemestre/{formsemestre_id}/resultats",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 200
    res = r.json()
    with open(
        "tests/ressources/results/formsemestre_resultat.json", encoding="utf8"
    ) as f:
        json_reference = f.read()
    ref = json.loads(json_reference)
    with open("venv/res.json", "w", encoding="utf8") as f:
        json.dump(res, f)
    _compare_formsemestre_resultat(res, ref)


def _compare_formsemestre_resultat(res: list[dict], ref: list[dict]):
    """compare les deux dict de résultats de formsemestre_resultat"""
    assert len(res) == len(ref)  # même nb d'étudiants
    res.sort(key=lambda e: e["etudid"])
    ref.sort(key=lambda e: e["etudid"])
    for res_d, ref_d in zip(res, ref):
        assert sorted(res_d.keys()) == sorted(ref_d.keys())
        for k in res_d:
            # On passe les absences pour le moment (TODO: mise à jour assiduité à faire)
            if "nbabs" in k:
                continue
            assert res_d[k] == ref_d[k], f"values for key {k} differ."
