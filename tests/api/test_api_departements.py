# -*- coding: utf-8 -*-

"""Test API: départements

Utilisation :
    créer les variables d'environnement: (indiquer les valeurs
    pour le serveur ScoDoc que vous voulez interroger)

    export SCODOC_URL="https://scodoc.xxx.net/"
    export SCODOC_USER="xxx"
    export SCODOC_PASSWD="xxx"
    export CHECK_CERTIFICATE=0 # ou 1 si serveur de production avec certif SSL valide

    (on peut aussi placer ces valeurs dans un fichier .env du répertoire tests/api).

    Lancer :
        pytest tests/api/test_api_departements.py
"""

import requests

from tests.api.setup_test_api import (
    API_URL,
    CHECK_CERTIFICATE,
    GET,
    POST_JSON,
    api_headers,
    api_admin_headers,
)
from tests.api.tools_test_api import (
    verify_fields,
    DEPARTEMENT_FIELDS,
    FORMSEMESTRE_FIELDS,
    verify_occurences_ids_etuds,
)
from app.scodoc import sco_utils as scu


def test_create_dept(api_admin_headers):
    """
    Routes: /departement/create,
            /departement/<string:dept_acronym>/edit
            /departement/<string:dept_acronym>/delete
    """
    dept = POST_JSON(
        "/departement/create",
        {"acronym": "XTEST", "visible": True},
        headers=api_admin_headers,
    )
    dept_r = GET(f"/departement/{dept['acronym']}", headers=api_admin_headers)
    assert dept["acronym"] == dept_r["acronym"]
    assert dept_r["visible"] is True
    dept_e = POST_JSON(
        f"/departement/{dept['acronym']}/edit",
        {"visible": False},
        headers=api_admin_headers,
    )
    dept_r = GET(f"/departement/{dept['acronym']}", headers=api_admin_headers)
    assert dept_r["visible"] is False
    r = POST_JSON(
        f"/departement/{dept['acronym']}/delete",
        headers=api_admin_headers,
    )
    assert r["OK"] is True


def test_departements(api_headers):
    """
    Routes: /departements_ids, /departement, /departement/<string:dept>/formsemestres_ids
    """
    # --- departement_ids : liste des ids
    r = requests.get(
        API_URL + "/departements_ids",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 200
    departements_ids = r.json()
    assert isinstance(departements_ids, list)
    assert len(departements_ids) > 0
    assert all(isinstance(x, int) for x in departements_ids)

    all_unique = True
    for id in departements_ids:
        if departements_ids.count(id) > 1:
            all_unique = False

    assert all_unique is True

    dept_id = departements_ids[0]

    # --- departement
    # Infos sur un département, accès par id
    r = requests.get(
        f"{API_URL}/departement/id/{dept_id}",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 200
    dept_a = r.json()
    # Infos sur un département, accès par acronyme4
    r = requests.get(
        f"{API_URL}/departement/{dept_a['acronym']}",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 200
    dept_b = r.json()

    assert dept_a == dept_b
    assert verify_fields(dept_a, DEPARTEMENT_FIELDS) is True
    assert isinstance(dept_a["id"], int)
    assert isinstance(dept_a["acronym"], str)
    assert dept_a["description"] is None or isinstance(dept_a["description"], str)
    assert isinstance(dept_a["visible"], bool)
    assert dept_a["date_creation"] is None or isinstance(dept_a["date_creation"], str)

    # --- departements : Liste des départements
    r = requests.get(
        API_URL + "/departements",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 200

    # --- formsemestre_ids : listes des ids de formsemestres du département
    r = requests.get(
        f"{API_URL}/departement/{dept_a['acronym']}/formsemestres_ids",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 200
    dept_ids_a = r.json()

    r = requests.get(
        f"{API_URL}/departement/id/{dept_a['id']}/formsemestres_ids",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 200
    dept_ids_b = r.json()

    assert dept_ids_a == dept_ids_b
    assert isinstance(dept_ids_a, list)
    assert all(isinstance(id, int) for id in dept_ids_a)
    assert len(dept_ids_a) > 0
    assert dept_id in dept_ids_a

    # Les erreurs
    id_inexistant = 50000
    r = requests.get(
        f"{API_URL}/departement/{id_inexistant}",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 404

    r = requests.get(
        f"{API_URL}/departement/{id_inexistant}/formsemestres_ids",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 404

    acronym_inexistant = "AAAAAAAAAAAAAAAAAAA"
    r = requests.get(
        f"{API_URL}/departement/{acronym_inexistant}",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 404

    r = requests.get(
        f"{API_URL}/departement/{acronym_inexistant}/formsemestres_ids",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 404


def test_list_etudiants(api_headers):
    fields = {"id", "code_nip", "code_ine", "nom", "nom_usuel", "prenom", "civilite"}

    r = requests.get(
        API_URL + "/departement/TAPI/etudiants",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 200
    etud_a = r.json()[0]

    r = requests.get(
        API_URL + "/departement/id/1/etudiants",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 200
    etud_b = r.json()[0]

    assert etud_a == etud_b
    assert verify_fields(etud_a, fields) is True
    assert isinstance(etud_a["id"], int)
    assert etud_a["code_nip"] is None or isinstance(etud_a["code_nip"], str)
    assert etud_a["code_ine"] is None or isinstance(etud_a["code_ine"], str)
    assert etud_a["nom"] is None or isinstance(etud_a["nom"], str)
    assert etud_a["nom_usuel"] is None or isinstance(etud_a["nom_usuel"], str)
    assert etud_a["prenom"] is None or isinstance(etud_a["prenom"], str)
    assert isinstance(etud_a["civilite"], str)
    assert len(etud_a["civilite"]) == 1

    all_unique = verify_occurences_ids_etuds(r.text)
    assert all_unique is True

    # Les erreurs
    id_inexistant = 50000
    r = requests.get(
        f"{API_URL}/departement/{id_inexistant}/etudiants",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 404

    acronym_inexistant = "AAAAAAAAAAAAAAAAAAA"
    r = requests.get(
        f"{API_URL}/departement/{acronym_inexistant}/etudiants",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 404


# liste_semestres_courant
def test_semestres_courant(api_headers):
    dept_id = 1
    r = requests.get(
        f"{API_URL}/departement/id/{dept_id}",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 200
    dept = r.json()
    assert dept["id"] == dept_id

    # Accès via acronyme
    r = requests.get(
        f"{API_URL}/departement/{dept['acronym']}/formsemestres_courants?date_courante=2022-07-01",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 200
    result_a = r.json()

    # accès via dept_id
    r = requests.get(
        f"{API_URL}/departement/id/{dept['id']}/formsemestres_courants?date_courante=2022-07-01",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 200
    result_b = r.json()
    assert result_a == result_b
    assert isinstance(result_a, list)  # liste de formsemestres
    assert len(result_a) > 0
    sem = result_a[0]
    assert verify_fields(sem, FORMSEMESTRE_FIELDS) is True
