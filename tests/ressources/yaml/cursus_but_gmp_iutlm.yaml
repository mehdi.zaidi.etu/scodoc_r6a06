# Tests unitaires jury BUT - IUTLM GMP
# Essais avec un BUT GMP, 4 UE + 1 bonus et deux parcours sur S3 S4
# Contrib Martin M.
#
# Pour ne jouer que ce scénario: 
#       pytest -m lemans tests/unit/test_but_jury.py

ReferentielCompetences:
  filename: but-GMP-05012022-081650.xml
  specialite: GMP

Formation:
  filename: scodoc_formation_BUT_GMP_lm.xml
  # Association des UE aux compétences:
  ues:
    # S1 : Tronc commun GMP
    'UE1.1-C1':
      annee: BUT1
      competence: Spécifier
    'UE1.2-C2':
      annee: BUT1
      competence: Développer
    'UE1.3-C3':
      annee: BUT1
      competence: Réaliser
    'UE1.4-C4':
      annee: BUT1
      competence: Exploiter    

    # S2 : Tronc commun GMP
    'UE2.1-C1':
      annee: BUT1
      competence: Spécifier
    'UE2.2-C2':
      annee: BUT1
      competence: Développer
    'UE2.3-C3':
      annee: BUT1
      competence: Réaliser
    'UE2.4-C4':
      annee: BUT1
      competence: Exploiter

    # S3 : Tronc commun GMP
    'UE3.1-C1':
      annee: BUT2
      competence: Spécifier
    'UE3.2-C2':
      annee: BUT2
      competence: Développer
    'UE3.3-C3':
      annee: BUT2
      competence: Réaliser
    'UE3.4-C4':
      annee: BUT2
      competence: Exploiter
    # S3 : Parcours II
    'UE3.5.IPI':
      annee: BUT2
      competence: Innover
      parcours: II
    # S3 : Parcour SNRV
    'UE3.5.SNRV':
      annee: BUT2
      competence: Virtualiser
      parcours: SNRV

    # S4 : Tronc commun GMP
    'UE4.1-C1':
      annee: BUT2
      competence: Spécifier
    'UE4.2-C2':
      annee: BUT2
      competence: Développer
    'UE4.3-C3':
      annee: BUT2
      competence: Réaliser
    'UE4.4-C4':
      annee: BUT2
      competence: Exploiter
    # S4 : Parcours II
    'UE4.5.II':
      annee: BUT2
      competence: Innover
      parcours: II
    # S4 : Parcour SNRV
    'UE4.5.SNRV':
      annee: BUT2
      competence: Virtualiser
      parcours: SNRV

  modules_parcours:
    # cette section permet d'associer des modules à des parcours
    # les codes modules peuvent être des regexp
    II: [ .*II.* ]
    SNRV: [ .*SNRV.* ] 

FormSemestres:
  # S1 et S2 :
  S1 :
    idx: 1
    date_debut: 2022-09-01
    date_fin: 2023-01-15
  S2 : 
    idx: 2
    date_debut: 2023-01-16
    date_fin: 2023-06-30
  # S3 avec les deux parcours réunis:
  S3:
    idx: 3
    date_debut: 2023-09-01
    date_fin: 2024-01-13
    codes_parcours: ['II', 'SNRV']
  # Un S1 pour les redoublants
  S1-red:
    idx: 1
    date_debut: 2023-09-02
    date_fin: 2024-01-12

Etudiants:
  gmp01: # cursus S1, S2, S3
    prenom: etugmp01
    civilite: M
    formsemestres:
      S1:
        notes_modules: # on joue avec les SAE seulement car elles sont "diagonales"
          "SAE1.1": 11.8
          "SAE1.2": 14.30
          "SAE1.3": 14.45
          "SAE1.4": 9.6
        attendu: # les codes jury que l'on doit vérifier
          deca:
            passage_de_droit: False
            nb_competences: 4
            nb_rcue_annee: 0
            decisions_ues:
              "UE1.1-C1":
                codes: [ "ADM", "..." ]
                moy_ue: 11.8
              "UE1.2-C2":
                codes: [ "ADM", "..." ]
                moy_ue: 14.30
              "UE1.3-C3":
                codes: [ "ADM", "..." ]
                moy_ue: 14.45
              "UE1.4-C4":
                codes: [ "AJ", "..." ]
                moy_ue: 9.6

      S2:
        notes_modules: # on joue avec les SAE seulement car elles sont "diagonales"
          "SAE2.01": 10.08
          "SAE2.02": 07.14 
          "SAE2.03": 10.67 
          "SAE2.04": 08.55
        attendu: # les codes jury que l'on doit vérifier
          deca:
            passage_de_droit: True
            nb_competences: 4
            nb_rcue_annee: 4
            valide_moitie_rcue: True
            codes: [ "PASD", "..." ]
            decisions_ues:
              "UE2.1-C1":
                codes: [ "ADM", "..." ]
                code_valide: ADM
                moy_ue: 10.08
              "UE2.2-C2":
                codes: [ "CMP", "..." ]
                code_valide: CMP
                moy_ue: 07.14
              "UE2.3-C3":
                codes: [ "ADM", "..." ]
                code_valide: ADM
                moy_ue: 10.67
              "UE2.4-C4":
                codes: [ "AJ", "..." ]
                code_valide: AJ
                decision_jury: ADJ # le jury force la décision ADJ
                moy_ue: 08.55
            decisions_rcues: # on repère ici les RCUE par l'acronyme de leur 1ere UE (donc du S1)
              "UE1.1-C1":
                code_valide: ADM
                rcue:
                  moy_rcue: 10.94
                  est_compensable: False
              "UE1.2-C2":
                code_valide: CMP
                rcue:
                  moy_rcue: 10.72
                  est_compensable: True
              "UE1.3-C3":
                code_valide: ADM
                rcue:
                  moy_rcue: 12.56
                  est_compensable: False
              "UE1.4-C4":
                code_valide: AJ
                rcue:
                  moy_rcue: 9.075
                  est_compensable: False



      S3:
        parcours: SNRV # Inscrit dans le parcours SNRV
        notes_modules: # on joue avec les SAE seulement car elles sont "diagonales"
          "S3.01": 9
          "S3.SNRV.02": 12.5
        attendu: # les codes jury que l'on doit vérifier
          deca:
            passage_de_droit: False
            nb_competences: 5 # 4 de Tronc Commun + 1 de parcours
            nb_rcue_annee: 0
            decisions_ues:
              "UE3.1-C1":
                codes: [ "AJ", "..." ]
                code_valide: AJ
                moy_ue: 9
              "UE3.2-C2":
                codes: [ "AJ", "..." ]
                code_valide: AJ
                moy_ue: 9
              "UE3.3-C3":
                codes: [ "AJ", "..." ]
                code_valide: AJ
                moy_ue: 9
              "UE3.4-C4":
                codes: [ "AJ", "..." ]
                code_valide: AJ
                moy_ue: 9
              "UE3.5.SNRV":
                codes: [ "ADM", "..." ]
                code_valide: ADM
                moy_ue: 12.5

  gmp02: # cursus S1, S2, S3
    prenom: etugmp02
    civilite: F
    formsemestres:
      S1:
        notes_modules: # on joue avec les SAE seulement car elles sont "diagonales"
          "SAE1.1": 14.5
          "SAE1.2": 13.2
          "SAE1.3": 9.5
          "SAE1.4": 8.7
        attendu: # les codes jury que l'on doit vérifier
          deca:
            passage_de_droit: False
            nb_competences: 4
            nb_rcue_annee: 0
            decisions_ues:
              "UE1.1-C1":
                codes: [ "ADM", "..." ]
                moy_ue: 14.5
              "UE1.2-C2":
                codes: [ "ADM", "..." ]
                moy_ue: 13.2
              "UE1.3-C3":
                codes: [ "AJ", "..." ]
                moy_ue: 9.5
              "UE1.4-C4":
                codes: [ "AJ", "..." ]
                moy_ue: 8.7

      S2:
        notes_modules: # on joue avec les SAE seulement car elles sont "diagonales"
          "SAE2.01": 14.4
          "SAE2.02": 17.8
          "SAE2.03": 11.2
          "SAE2.04": 9.2
        attendu: # les codes jury que l'on doit vérifier
          deca:
            #passage_de_droit: true
            nb_competences: 4
            nb_rcue_annee: 4
            #res_pair: None
            valide_moitie_rcue: true
            codes: [ "PASD", "..." ]
            decisions_ues:
              "UE2.1-C1":
                codes: [ "ADM", "..." ]
                code_valide: ADM
                moy_ue: 14.4
              "UE2.2-C2":
                codes: [ "ADM", "..." ]
                code_valide: ADM
                moy_ue: 17.8
              "UE2.3-C3":
                codes: [ "ADM", "..." ]
                code_valide: ADM
                moy_ue: 11.2
              "UE2.4-C4":
                codes: [ "AJ", "..." ]
                code_valide: AJ
                moy_ue: 9.2
            decisions_rcues: # on repère ici les RCUE par l'acronyme de leur 1ere UE
              "UE1.1-C1":
                code_valide: ADM
                rcue:
                  moy_rcue: 14.45
                  est_compensable: False
              "UE1.2-C2":
                code_valide: ADM
                rcue:
                  moy_rcue: 15.5
                  est_compensable: False
              "UE1.3-C3":
                code_valide: CMP
                rcue:
                  moy_rcue: 10.35
                  est_compensable: True
              "UE1.4-C4":
                code_valide: AJ
                rcue:
                  moy_rcue: 8.95
                  est_compensable: False



      S3:
        notes_modules: # on joue avec les SAE seulement car elles sont "diagonales"
          "S3.01": 12
          "S3.SNRV.02": 14
        attendu: # les codes jury que l'on doit vérifier
          deca:
            passage_de_droit: False
            nb_competences: 4 # et non 5 car pas inscrit à un parcours
            nb_rcue_annee: 0
            decisions_ues:
              "UE3.1-C1":
                codes: [ "ADM", "..." ]
                code_valide: ADM
                moy_ue: 12
              "UE3.2-C2":
                codes: [ "ADM", "..." ]
                code_valide: ADM
                moy_ue: 12
              "UE3.3-C3":
                codes: [ "ADM", "..." ]
                code_valide: ADM
                moy_ue: 12
              "UE3.4-C4":
                codes: [ "ADM", "..." ]
                code_valide: ADM
                moy_ue: 12
              # "UE3.5.SNRV":
              #   codes: [ "ADM", "..." ]
              #   code_valide: ADM
              #   moy_ue: 14

  gmp03: # cursus S1, S2, S1-red
    prenom: etugmp03
    civilite: X
    formsemestres:
      S1:
        notes_modules: # on joue avec les SAE seulement car elles sont "diagonales"
          "SAE1.1": 12.7
          "SAE1.2": 8.4
          "SAE1.3": 10.1
          "SAE1.4": 9.8
        attendu: # les codes jury que l'on doit vérifier
          deca:
            passage_de_droit: False
            nb_competences: 4
            nb_rcue_annee: 0
            decisions_ues:
              "UE1.1-C1":
                codes: [ "ADM", "..." ]
                moy_ue: 12.7
              "UE1.2-C2":
                codes: [ "AJ", "..." ]
                moy_ue: 8.4
              "UE1.3-C3":
                codes: [ "ADM", "..." ]
                moy_ue: 10.1
              "UE1.4-C4":
                codes: [ "AJ", "..." ]
                moy_ue: 9.8

      S2:
        notes_modules: # on joue avec les SAE seulement car elles sont "diagonales"
          "SAE2.01": 10.2
          "SAE2.02": 9.6
          "SAE2.03": 14.3
          "SAE2.04": 8.4
        attendu: # les codes jury que l'on doit vérifier
          deca:
            nb_competences: 4 # et non 5 car pas inscrit à un parcours
            nb_rcue_annee: 4
            valide_moitie_rcue: false
            codes: [ "RED", "..." ]
            decisions_ues:
              "UE2.1-C1":
                codes: [ "ADM", "..." ]
                code_valide: ADM
                moy_ue: 10.2
              "UE2.2-C2":
                codes: [ "AJ", "..." ]
                code_valide: AJ
                moy_ue: 9.6
              "UE2.3-C3":
                codes: [ "ADM", "..." ]
                code_valide: ADM
                moy_ue: 14.3
              "UE2.4-C4":
                codes: [ "AJ", "..." ]
                code_valide: AJ
                moy_ue: 8.4
            decisions_rcues: # on repère ici les RCUE par l'acronyme de leur 1ere UE (du S1 donc)
              "UE1.1-C1":
                code_valide: ADM
                rcue:
                  moy_rcue: 11.45
                  est_compensable: False
              "UE1.2-C2":
                code_valide: AJ
                rcue:
                  moy_rcue: 9
                  est_compensable: False
              "UE1.3-C3":
                code_valide: ADM
                rcue:
                  moy_rcue: 12.2
                  est_compensable: False
              "UE1.4-C4":
                code_valide: AJ
                rcue:
                  moy_rcue: 9.1
                  est_compensable: False
      S1-red:
        # On a capitalisé les UE/RCUE UE1.1-C1 et UE1.3-C3
        # L'étudiant décide de refaire qd même l'UE UE1.1-C1
        notes_modules: # on ne note ici que les UE à refaire
          "SAE1.1": 14. # il améliore son UE 1
          "SAE1.2": 12. # et cette fois reussi les autres
          "SAE1.3": EXC # pour que l'éval soit complete
          "SAE1.4": 13.
        attendu:
          nb_competences: 4
          nb_rcue_annee: 0
          decisions_ues:
            "UE1.1-C1":
              code_valide: ADM
              moy_ue: 14 # nouvelle moyenne
            "UE1.2-C2":
              code_valide: ADM
              moy_ue: 12
            "UE1.3-C3":
              moy_ue: 10.1 # capitalisée du S1 précédent XXX à vérifier
            "UE1.4-C4":
              code_valide: ADM
              moy_ue: 13

  gmp04: # cursus S1-red (primo-entrant)
    prenom: Primo
    civilite: M
    formsemestres:
      S1-red:
        notes_modules:
          "SAE1.1": 11.
          "SAE1.2": 12.
          "SAE1.3": 13.
          "SAE1.4": 9.
        attendu: # les codes jury que l'on doit vérifier
          deca:
            "UE1.4-C4":
                code_valide: "AJ"
                moy_ue: 9.
