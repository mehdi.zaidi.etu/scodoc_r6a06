# -*- coding: UTF-8 -*
"""ScoDoc Flask views
"""
import datetime

from flask import Blueprint
from flask import g, current_app, request
from flask_login import current_user

from app import db
from app.models import Identite
from app.models.formsemestre import FormSemestre
from app.scodoc import notesdb as ndb
from app.scodoc import sco_assiduites
from app.scodoc import sco_formsemestre_status
from app.scodoc import sco_preferences
from app.scodoc.html_sidebar import retreive_formsemestre_from_request
from app.scodoc.sco_permissions import Permission
from app.scodoc import sco_utils as scu
import sco_version

scodoc_bp = Blueprint("scodoc", __name__)
scolar_bp = Blueprint("scolar", __name__)
notes_bp = Blueprint("notes", __name__)
users_bp = Blueprint("users", __name__)
absences_bp = Blueprint("absences", __name__)
assiduites_bp = Blueprint("assiduites", __name__)


# Cette fonction est bien appelée avant toutes les requêtes
# de tous les blueprints
# mais apparemment elle n'a pas acces aux arguments
@scodoc_bp.before_app_request
def start_scodoc_request():
    """Affecte toutes les requêtes, de tous les blueprints"""
    # current_app.logger.info(f"start_scodoc_request")
    ndb.open_db_connection()
    if current_user and current_user.is_authenticated:
        current_user.last_seen = datetime.datetime.utcnow()
        db.session.commit()
    # caches locaux (durée de vie=la requête en cours)
    g.stored_get_formsemestre = {}
    # g.stored_etud_info = {} optim en cours, voir si utile


@scodoc_bp.teardown_app_request
def close_dept_db_connection(arg):
    # current_app.logger.info("close_db_connection")
    ndb.close_db_connection()


class ScoData:
    """Classe utilisée pour passer des valeurs aux vues (templates)"""

    def __init__(self, etud: Identite = None, formsemestre: FormSemestre = None):
        # Champs utilisés par toutes les pages ScoDoc (sidebar, en-tête)
        self.Permission = Permission
        self.scu = scu
        self.SCOVERSION = sco_version.SCOVERSION
        # -- Informations étudiant courant, si sélectionné:
        if etud is None:
            etudid = g.get("etudid", None)
            if etudid is None:
                if request.method == "GET":
                    etudid = request.args.get("etudid", None)
                elif request.method == "POST":
                    etudid = request.form.get("etudid", None)
            if etudid is not None:
                etud = Identite.get_etud(etudid)
        self.etud = etud
        if etud is not None:
            # Infos sur l'étudiant courant
            ins = self.etud.inscription_courante()
            if ins:
                self.etud_cur_sem = ins.formsemestre
                (
                    self.nbabs,
                    self.nbabsjust,
                ) = sco_assiduites.get_assiduites_count_in_interval(
                    etud.id,
                    self.etud_cur_sem.date_debut.isoformat(),
                    self.etud_cur_sem.date_fin.isoformat(),
                    scu.translate_assiduites_metric(
                        sco_preferences.get_preference("assi_metrique")
                    ),
                )
                self.nbabsnj = self.nbabs - self.nbabsjust
            else:
                self.etud_cur_sem = None
        else:
            self.etud = None
        # --- Informations sur semestre courant, si sélectionné
        if formsemestre is None:
            formsemestre_id = retreive_formsemestre_from_request()
            if formsemestre_id is not None:
                formsemestre = FormSemestre.get_formsemestre(formsemestre_id)
        if formsemestre is None:
            self.sem = None
            self.sem_menu_bar = None
        else:
            self.sem = formsemestre
            self.sem_menu_bar = sco_formsemestre_status.formsemestre_status_menubar(
                self.sem
            )
        self.formsemestre = formsemestre
        # --- Préférences
        # prefs fallback to global pref if sem is None:
        if formsemestre:
            formsemestre_id = formsemestre.id
        else:
            formsemestre_id = None
        self.prefs = sco_preferences.SemPreferences(formsemestre_id)


from app.views import (
    absences,
    assiduites,
    but_formation,
    notes_formsemestre,
    notes,
    pn_modules,
    refcomp,
    scodoc,
    scolar,
    users,
    user_board,
)
