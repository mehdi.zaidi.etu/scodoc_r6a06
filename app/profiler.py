from time import time
from datetime import datetime


class Profiler:
    OUTPUT: str = "/tmp/scodoc.profiler.csv"

    def __init__(self, tag: str) -> None:
        self.tag: str = tag
        self.start_time: time = None
        self.stop_time: time = None

    def start(self):
        self.start_time = time()
        return self

    def stop(self):
        self.stop_time = time()
        return self

    def elapsed(self) -> float:
        return self.stop_time - self.start_time

    def dates(self) -> tuple[datetime, datetime]:
        return datetime.fromtimestamp(self.start_time), datetime.fromtimestamp(
            self.stop_time
        )

    def write(self):
        with open(Profiler.OUTPUT, "a") as file:
            dates: tuple = self.dates()
            date_str = (dates[0].isoformat(), dates[1].isoformat())
            file.write(f"\n{self.tag},{self.elapsed() : .2}")

    @classmethod
    def write_in(cls, msg: str):
        with open(cls.OUTPUT, "a") as file:
            file.write(f"\n# {msg}")

    @classmethod
    def clear(cls):
        with open(cls.OUTPUT, "w") as file:
            file.write("")
