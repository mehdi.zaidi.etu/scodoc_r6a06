// Affiche et met a jour la liste des UE partageant le meme code

$().ready(function () {
  if (document.querySelector("#tf_ue_id")) {
    /* fonctions spécifiques pour edition UE */
    update_ue_list();
    $("#tf_ue_code").bind("keyup", update_ue_list);

    $("select#tf_type").change(function () {
      update_bonus_description();
    });
    update_bonus_description();
  }
});

function update_bonus_description() {
  var ue_type = $("#tf_type")[0].value;
  if (ue_type == "1") {
    /* UE SPORT */
    $("#bonus_description").show();
    var query = "/ScoDoc/get_bonus_description/default";
    $.get(query, "", function (data) {
      $("#bonus_description").html(data);
    });
  } else {
    $("#bonus_description").html("");
    $("#bonus_description").hide();
  }
}

function update_ue_list() {
  let ue_id = $("#tf_ue_id")[0].value;
  let ue_code = $("#tf_ue_code")[0].value;
  let query =
    SCO_URL +
    "/Notes/ue_sharing_code?ue_code=" +
    ue_code +
    "&hide_ue_id=" +
    ue_id +
    "&ue_id=" +
    ue_id;
  $.get(query, "", function (data) {
    $("#ue_list_code").html(data);
  });
}

function set_ue_parcour(checkbox) {
  let url = checkbox.dataset.setter;
  const checkboxes = document.querySelectorAll(
    '#choix_parcours input[type="checkbox"]:checked'
  );
  const parcours_ids = [];
  checkboxes.forEach(function (checkbox) {
    parcours_ids.push(checkbox.value);
  });

  fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(parcours_ids),
  })
    .then((response) => response.json())
    .then((data) => {
      if (data.status == 404) {
        sco_error_message(data.message);
      } else {
        sco_message(data.message);
      }
    });
}
