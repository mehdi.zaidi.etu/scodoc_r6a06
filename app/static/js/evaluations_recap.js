// Tableau recap evaluations du semestre
$(function () {
  $("table.evaluations_recap").DataTable({
    paging: false,
    searching: true,
    info: false,
    autoWidth: false,
    fixedHeader: {
      header: true,
      footer: false,
    },
    orderCellsTop: true, // cellules ligne 1 pour tri
    aaSorting: [], // Prevent initial sorting
    colReorder: true,
    columnDefs: [
      {
        // colonne date, triable (XXX ne fonctionne pas)
        targets: ["date"],
        type: "string",
      },
    ],
    dom: "Bfrtip",
    buttons: [
      {
        extend: "copyHtml5",
        text: "Copier",
        exportOptions: { orthogonal: "export" },
      },
      {
        extend: "excelHtml5",
        exportOptions: { orthogonal: "export" },
        title: document.querySelector("table.evaluations_recap").dataset
          .filename,
      },
    ],
  });
});
