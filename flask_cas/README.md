# Flask-CAS

Forked from <https://github.com/MasterRoshan/flask-cas-ng>
and adapted by Emmanuel Viennet, Feb. 2023.

- logout: clear `_CAS_TOKEN`.
- Use `url` instead of `service` parameter in logout URL.
